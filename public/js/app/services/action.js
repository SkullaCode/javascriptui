/**
 * -- Action --
 * Add a method to the action executor if a form needs a non-standard way of sending the
 * request to the server and handling the response. methods can be triggered by inline
 * onclick and onchange events or by the data-action attribute on a submit button.
 * please note that the data-action is the same item that determines the location of  
 * the form if the submit button is not contained within it.
 */

